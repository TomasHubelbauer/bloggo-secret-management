# Secret Management

- Use [Kingston DataTraveler 2000](https://www.kingston.com/en/usb/encrypted_security/dt2000) with a strong code

## Mounting USB in WSL

[File System Improvements to the Windows Subsystem for Linux](https://blogs.msdn.microsoft.com/wsl/2017/04/18/file-system-improvements-to-the-windows-subsystem-for-linux/)

Assuming a Windows drive letter is `D`:

```sh
sudo mkdir /mnt/d
sudo mount -t drvfs D: /mnt/d
cd /mnt/d
```

When done, use `sudo umount /mnt/d` (**`umount`**, not `unmount`) and eject the device in Windows.

## Backing up

Use two DT2000s.

`rsync /mnt/d /mnt/e`

## [Using GPG](https://askubuntu.com/a/424193/459660)

- Generate and encrypt a new passphrase: `pwgen 20 | tee /dev/tty | gpg -c > passphrase`
- Encrypt an existing passphrase from a paste: `echo "passphrase" | gpg -c > passphrase`
- Encrypt an existing passphrase from a file: `gpg -c file > passphrase`
- Decrypt a passphrase: `gpg -d passphrase`

### GPG with files

- `gpg -c credentials.md` creates `credentials.md.gpg`
- `gpg credentials.md.gpg` creates `credentials.md`

## [Using OpenSSL](https://askubuntu.com/a/160334/459660)

[Here's why I prefer to use GPG](https://stackoverflow.com/a/28248800/2715716)

- Generate and encrypt a new passphrase: `pwgen 20 | tee /dev/tty | openssl aes-128-cbc > passphrase`
- Encrypt an existing passphrase from a paste: `echo "passphrase" | openssl aes-128-cbc > passphrase`
- Encrypt an existing passphrase from a file: `openssl aes-128-cbc < file > passphrase`
- Decrypt a passphrase: `openssl aes-128-cbc -d < passphrase`

## Using [`pass`](https://www.passwordstore.org/)

It appears as though this cannot operator off a mounted drive. It always uses `~` as the store directory.
